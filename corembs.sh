#!/bin/bash

PATH_TO_FILE=$1

if [[ ! ffmpeg ]]; then
    echo "Error: ffmpeg does not exist. Please, install it" && exit 1
elif [[ -z $(ffmpeg -i "$PATH_TO_FILE" 2>&1 >/dev/null | grep Subtitle) ]]; then
    echo "Error: file does not have subtitles" && exit 1
fi

FILENAME=$(basename "$PATH_TO_FILE" | sed "s/\..*//")
DIRNAME=$(dirname "$PATH_TO_FILE")
SUBTITLE_FILE="${DIRNAME}/${FILENAME}.sub.ssa"

ffmpeg -i "$DIRNAME/$FILENAME.mkv" -map 0:s -c:s copy "$SUBTITLE_FILE"
sed -i "s/Original Script.*//" "$SUBTITLE_FILE" 

ffmpeg -i "$DIRNAME/$FILENAME.mkv" -i "$SUBTITLE_FILE" -map 0:v -c:v copy -map 0:a -c:a copy -map 1:s -c:s copy "$DIRNAME/$FILENAME-fixed.mkv"
rm "$SUBTITLE_FILE"

