#!/bin/bash

PATH_TO_SUBTITLES="$1"

if [[ -z $PATH_TO_SUBTITLES ]]; then
    echo "Path to subtitles does not specified!" && exit 1
fi

find $PATH_TO_SUBTITLES -type f -exec sed -i 's/Original Script.*//' {} \;

