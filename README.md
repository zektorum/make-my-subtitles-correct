# make-my-subtitles-correct
This repository is collection of scripts help you to fix subtitles from Crunchyroll in VLC.

## corsub.sh
This script fixes external subtitles. Use
```
./corsub.sh PATH_TO_SUBTITLES
```
to fix your ssa/ass subtitles.

